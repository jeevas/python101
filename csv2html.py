import csv, sys

csv_filename = sys.argv[1]
core_filename = csv_filename[0]
html_filename = csv_filename.split(".")[0]+".html"

with open(csv_filename, 'rb') as input, open (html_filename, 'w+') as output:
    readit = csv.reader(input)
    for row in readit:
        output.write(str(row))
        
print "Type 'open %s'to open the html file" % (html_filename)

